# AI Object Detection

For CEI object detection task.

---
## Journal

*	2018/8/10
	*	New scripts for auto-download checkpoints & VOC 2007/2012 trainval/test data
	*	New script for convert data & do evalution
*	2018/8/9
	*	Copy SSD-tensorflow codebase from https://github.com/balancap/SSD-Tensorflow

---
## Dataset

Dataset download:

*	VOC 2007
	*	[training/validation data](http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar) (450MB tar file)
	*	[annotated test data](http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar) (430MB tar file)
*	VOC 2012
	*	[training/validation data](http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar) (2GB tar file)
	*	[Test Data](http://pjreddie.com/media/files/VOC2012test.tar) (1.8 GB)

---
## Checkpoints

Checkpoints download:

*	[SSD-300 VGG-based (VOC07+12 trainval)](https://drive.google.com/open?id=0B0qPCUZ-3YwWZlJaRTRRQWRFYXM)
*	[SSD-300 VGG-based (VOC07+12+COCO trainval)](https://drive.google.com/file/d/0B0qPCUZ-3YwWUXh4UHJrd1RDM3c/view?usp=sharing)
*	[SSD-512 VGG-based (VOC07+12+COCO trainval)](https://drive.google.com/open?id=0B0qPCUZ-3YwWT1RCLVZNN3RTVEU)