# Setting
PYTHON=python3

###########################################################
# Pascal VOC 2007 train validation set
#DATASET_DIR=./dataset/VOC/2007/trainval/VOCdevkit/VOC2007/
#OUTPUT_NAME=voc_2007_train

# Pascal VOC 2007 test set
DATASET_DIR=./dataset/VOC/2007/test/VOCdevkit/VOC2007/
OUTPUT_NAME=voc_2007_test

# Pascal VOC 2012 train validation set
#DATASET_DIR=./dataset/VOC/2012/trainval/VOCdevkit/VOC2012/
#OUTPUT_NAME=voc_2012_train

# Pascal VOC 2012 test set
#DATASET_DIR=./dataset/VOC/2012/test/VOCdevkit/VOC2012/
#OUTPUT_NAME=voc_2012_test
###########################################################

###########################################################
# VGG SSD300 VOC07+12 checkpoint data
#CHECKPOINT_PATH=./SSD-Tensorflow/checkpoints/ssd300_voc07_12/VGG_VOC0712_SSD_300x300_iter_120000.ckpt

# VGG SSD300 VOC07+12+COCO checkpoint data
CHECKPOINT_PATH=./SSD-Tensorflow/checkpoints/ssd300_voc07_12_coco/VGG_VOC0712_SSD_300x300_ft_iter_120000.ckpt

# VGG SSD512_VOC07+12+COCO checkpoint data
#CHECKPOINT_PATH=./SSD-Tensorflow/checkpoints/ssd512_voc07_12_coco/VGG_VOC0712_SSD_512x512_ft_iter_120000.ckpt
###########################################################

# Convert dataset to "TF-Records" format
echo -e '##### Convert dataset to TF-Records format #####'

rm ${DATASET_DIR}/*.tfrecord

${PYTHON} ./SSD-Tensorflow/tf_convert_data.py \
    --dataset_name=pascalvoc \
    --dataset_dir=${DATASET_DIR} \
    --output_name=${OUTPUT_NAME} \
    --output_dir=${DATASET_DIR}

# Evalution
echo -e '##### Start to evalution #####'

${PYTHON} ./SSD-Tensorflow/eval_ssd_network.py \
    --eval_dir=${EVAL_DIR} \
    --dataset_dir=${DATASET_DIR} \
    --dataset_name=pascalvoc_2007 \
    --dataset_split_name=test \
    --model_name=ssd_300_vgg \
    --checkpoint_path=${CHECKPOINT_PATH} \
    --batch_size=1

# Train
echo -e '##### Start to train #####'

TRAIN_DIR=./SSD-Tensorflow/logs/

${PYTHON} ./SSD-Tensorflow/train_ssd_network.py \
	--train_dir=${TRAIN_DIR} \
	--dataset_dir=${DATASET_DIR} \
	--dataset_name=pascalvoc_2012 \
	--dataset_split_name=train \
	--model_name=ssd_300_vgg \
	--checkpoint_path=${CHECKPOINT_PATH} \
	--save_summaries_secs=60 \
	--save_interval_secs=600 \
	--weight_decay=0.0005 \
	--optimizer=adam \
	--learning_rate=0.001 \
	--batch_size=16
