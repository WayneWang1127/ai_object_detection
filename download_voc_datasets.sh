DATASET_PATH=./dataset

URL_VOC_2007_TEST='http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar'
URL_VOC_2007_TRAINVAL='http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar'
URL_VOC_2012_TEST='http://pjreddie.com/media/files/VOC2012test.tar'
URL_VOC_2012_TRAINVAL='http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar'

# Create folders
mkdir -p ${DATASET_PATH}/VOC/

# Download dataset
test -e ${DATASET_PATH}/VOC/voc_2007_test.tar || \
    wget ${URL_VOC_2007_TEST} -O ${DATASET_PATH}/VOC/voc_2007_test.tar

rm -rf ${DATASET_PATH}/VOC/2007/test/
mkdir -p ${DATASET_PATH}/VOC/2007/test/
tar -xf ${DATASET_PATH}/VOC/voc_2007_test.tar -C ${DATASET_PATH}/VOC/2007/test/

test -e ${DATASET_PATH}/VOC/voc_2007_trainavl.tar ||\
    wget ${URL_VOC_2007_TRAINVAL} -O ${DATASET_PATH}/VOC/voc_2007_trainavl.tar

rm -rf ${DATASET_PATH}/VOC/2007/trainval/
mkdir -p ${DATASET_PATH}/VOC/2007/trainval/
tar -xf ${DATASET_PATH}/VOC/voc_2007_trainavl.tar -C ${DATASET_PATH}/VOC/2007/trainval/

test -e ${DATASET_PATH}/VOC/voc_2012_test.tar || \
    wget ${URL_VOC_2012_TEST} -O ${DATASET_PATH}/VOC/voc_2012_test.tar

rm -rf ${DATASET_PATH}/VOC/2012/test/
mkdir -p ${DATASET_PATH}/VOC/2012/test/
tar -xf ${DATASET_PATH}/VOC/voc_2012_test.tar -C ${DATASET_PATH}/VOC/2012/test/

test -e ${DATASET_PATH}/VOC/voc_2012_trainval.tar || \
    wget ${URL_VOC_2012_TRAINVAL} -O ${DATASET_PATH}/VOC/voc_2012_trainval.tar

rm -rf ${DATASET_PATH}/VOC/2012/trainval/
mkdir -p ${DATASET_PATH}/VOC/2012/trainval/
tar -xf ${DATASET_PATH}/VOC/voc_2012_trainval.tar -C ${DATASET_PATH}/VOC/2012/trainval/