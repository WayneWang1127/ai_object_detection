CHECKPOINT_PATH=./SSD-Tensorflow/checkpoints

URL_SSD300_VOC07_12='https://drive.google.com/uc?export=download&id=0B0qPCUZ-3YwWZlJaRTRRQWRFYXM'
NAME_SSD300_VOC07_12=VGG_VOC0712_SSD_300x300_iter_120000.ckpt.zip

URL_SSD300_VOC07_12_COCO='https://drive.google.com/uc?export=download&id=0B0qPCUZ-3YwWUXh4UHJrd1RDM3c'
NAME_SSD300_VOC07_12_COCO=VGG_VOC0712_SSD_300x300_ft_iter_120000.ckpt.zip

URL_SSD512_VOC07_12_COCO='https://drive.google.com/uc?export=download&id=0B0qPCUZ-3YwWT1RCLVZNN3RTVEU'
NAME_SSD512_VOC07_12_COCO=VGG_VOC0712_SSD_512x512_ft_iter_120000.ckpt.zip

# Download SSD-300 VGG base, VOC07+12 trainval ckpt file
test -e ${CHECKPOINT_PATH}/${NAME_SSD300_VOC07_12} || \
    wget --no-check-certificate ${URL_SSD300_VOC07_12} -O ${CHECKPOINT_PATH}/${NAME_SSD300_VOC07_12}

rm -rf ${CHECKPOINT_PATH}/ssd300_voc07_12
mkdir -p ${CHECKPOINT_PATH}/ssd300_voc07_12
unzip ${CHECKPOINT_PATH}/${NAME_SSD300_VOC07_12} -d ${CHECKPOINT_PATH}/ssd300_voc07_12/

# Download SSD-300 VGG base, VOC07+12+COCO trainval ckpt file
test -e ${CHECKPOINT_PATH}/${NAME_SSD300_VOC07_12_COCO} || \
    wget --no-check-certificate ${URL_SSD300_VOC07_12_COCO} -O ${CHECKPOINT_PATH}/${NAME_SSD300_VOC07_12_COCO}

mkdir -p ${CHECKPOINT_PATH}/ssd300_voc07_12_coco
unzip ${CHECKPOINT_PATH}/${NAME_SSD300_VOC07_12_COCO} -d ${CHECKPOINT_PATH}/ssd300_voc07_12_coco/

# Download SSD-512 VGG base, VOC07+12+COCO trainval ckpt file
test -e ${CHECKPOINT_PATH}/${NAME_SSD512_VOC07_12_COCO} || \
    wget --no-check-certificate ${URL_SSD512_VOC07_12_COCO} -O ${CHECKPOINT_PATH}/${NAME_SSD512_VOC07_12_COCO}

mkdir -p ${CHECKPOINT_PATH}/ssd512_voc07_12_coco
unzip ${CHECKPOINT_PATH}/${NAME_SSD512_VOC07_12_COCO} -d ${CHECKPOINT_PATH}/ssd512_voc07_12_coco/